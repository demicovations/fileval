﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileVal;
namespace API_test {
   public class Program {
       public static void Main(string[] args) {
            var fi = new FileHandler(AppDomain.CurrentDomain.BaseDirectory + "testconfig.ini", Properties.Resources.original);
            var fs = new FileValues(fi, true);
            //fi.WriteDefaultStream();
            //FileHandlerTest(fi);
            FileValuesTest(fs);


            Console.ReadKey();
       }

        private static void FileHandlerTest(FileHandler fi) {

            var d = fi.GetFileDictionary();
            var lines = fi.FileLines;
            var dd = fi.GetDefaultFileDictionary();

            var s = fi.GetFileDictionaryFrom("Sec1");
            var ss = fi.GetFileDictionaryFrom("Sec3");
            //var sss = fi.GetFileDictionaryFrom("Sec4654");
            //var ssss = fi.GetFileDictionaryFrom("");
            //var sssss = fi.GetFileDictionaryFrom(null);

            var sec = fi.GetAllFileSections();
            var secc = fi.GetAllFileDefaultSections();

            var i = fi.GetKeyIndex("Sec1", "Pes");
            //var ii = fi.GetKeyIndex("Sec1", "Psfdsfes");
            //var iii = fi.GetKeyIndex("Sec3", "Pes");
            //var iiii = fi.GetKeyIndex("", "Pes");
            //var iiiii = fi.GetKeyIndex(null, "Pes");
            //var iiiiii = fi.GetKeyIndex("Sec3", "");
            //var iiiiiii = fi.GetKeyIndex("Sec3", null);
            //var iiiiiiii = fi.GetKeyIndex("", "");
            //var iiiiiiiii = fi.GetKeyIndex(null, null);

            //fi.WriteSingle("Sec1", "Pes", "1234");
            //fi.WriteSingle("Sec1", "Pes", "");
            //fi.WriteSingle("Sec14", "Pes", "1234");
            //fi.WriteSingle("", "Pes", "1234");
            //fi.WriteSingle(null, "Pes", "1234");
            //fi.WriteSingle("Sec1", "Pesss", "1234");
            //fi.WriteSingle("Sec1", "", "1234");
            //fi.WriteSingle("Sec1", null, "1234");
            //fi.WriteSingle(null, null, "1234");
            //fi.WriteSingle("", "", "1234");


            var list = new List<string>();
            list.Add("asdfasf");
            list.Add("assdfdfasf");
            list.Add("asdfasdfsf");
            list.Add("asdfasf");
            list.Add("asdfassssdff");
            list.Add("asdfassssssssssf");
            //fi.WriteList(list);
            //fi.WriteList(null);
            //fi.WriteList(new List<string>());
        }

        private static void FileValuesTest(FileValues fs) {

            //fs.SaveSingle("Sec1", "kocka", "1234");
            //fs.SaveSingle("asdfasdf", "kocka", "1234");
            //fs.SaveSingle("Sec1", "asdfasfd", "1234");
            //fs.SaveSingle("asdfasdf", "afasdf", "1234");
            //fs.SaveSingle("", "kocka", "1234");
            //fs.SaveSingle("Sec1", "", "1234");
            //fs.SaveSingle("", "", "1234");
            //fs.SaveSingle(null, null, "1234");
            //fs.SaveSingle("Sec1", "kocka", "");
            //fs.SaveSingle("Sec1", "kocka", null);

            //fs.SaveMultiple("Sec1", "kocka", "789");
            //fs.SaveMultiple("Sec1", "asdf", "789");
            //fs.SaveMultiple("Sec1", "", "789");
            //fs.RestoreDefalutFileStream();
        }
    }
}
