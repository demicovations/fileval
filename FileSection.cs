﻿using System;
using System.Collections.Generic;

namespace FileVal {
    /// <summary>
    /// Holds file section informations
    /// </summary>
    internal class FileSection {

        public FileSection(string sectionName, Dictionary<int,KeyValuePair<string, string>> keyValues, int sectionIndex) {
            if (sectionIndex <= 0) throw new IndexOutOfRangeException();

            foreach (var item in keyValues) {
                KeyIndexes.Add(item.Value.Key, item.Key);
                KeyValues.Add(item.Value.Key, item.Value.Value);
            }

            SectionIndex = sectionIndex;
            SectionName = sectionName;
        }

        public string SectionName { get; }

        public int SectionIndex { get; }
        public Dictionary<string, int> KeyIndexes { get; } = new Dictionary<string, int>();

        public Dictionary<string, string> KeyValues { get; private set; } = new Dictionary<string, string>();

        public void UpdateKey(string key, string newValue) {
            if (newValue == null) {
                newValue = string.Empty;
            }
            KeyValues[key] = newValue;
        }
    }
}