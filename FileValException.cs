﻿using System;
using System.Runtime.Serialization;

namespace FileVal {
    /// <summary>
    /// Represents a common exceptions with FileVal API
    /// </summary>
    [Serializable]
    public class FileValException : Exception {
        /// <summary>
        /// Enum for different exceptions types which might occur while using API
        /// </summary>
        public enum ExceptionType { 
            /// <summary>
            /// Section does not exist in scoped file
            /// </summary>
            SectionNotExist, 
            /// <summary>
            /// Key does not exist in scoped section
            /// </summary>
            KeyNotExist, 
            /// <summary>
            /// <see cref="FileVal.FileValues"/> is set to ReadOnly and no values in file can be changed
            /// </summary>
            ReadOnly,
            /// <summary>
            /// /// <see cref="FileVal.FileValues"/> is set to ReadOnly and file in location does not exist (it cannot be restored from default file thus is ReadOnly)
            /// </summary>
            ReadOnlyFileMissing, 
            /// <summary>
            /// Sections are duplicated in scoped file
            /// </summary>
            DuplicateSections, 
            /// <summary>
            /// Sections in file and default file are not matching
            /// </summary>
            SectionsNotMatchInDefaultStream, 
            /// <summary>
            /// Value from key does not meet certain conditions
            /// </summary>
            InvalidValue, 
            /// <summary>
            /// Key is duplicated in scoped section
            /// </summary>
            DuplicateKey 
        }
        /// <summary>
        /// Constructor for <see cref="FileValException"/>
        /// </summary>
        /// <param name="type">Reason why this exception is thrown</param>
        public FileValException(ExceptionType type) : base($"Type: {type}") {
            ThrownExceptionType = type;
        }
        /// <summary>
        /// Constructor for <see cref="FileValException"/>
        /// </summary>
        /// <param name="type">Reason why this exception is thrown</param>
        /// <param name="message">Custom message</param>
        public FileValException(ExceptionType type, string message) : base($"Message: {message} Type: {type}") {
            ThrownExceptionType = type;
        }
        /// <summary>
        /// Constructor for <see cref="FileValException"/>
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        protected FileValException(SerializationInfo info, StreamingContext context) : base(info, context) {

        }

        //============PUBLIC PROPERTY==============
        /// <summary>
        /// Parameter name which caused the exception
        /// </summary>
        public string ParameterName { get; set; }
        /// <summary>
        /// Reason why this exception was thrown
        /// </summary>
        public ExceptionType ThrownExceptionType { get; }
        //=========================================
    }
}
