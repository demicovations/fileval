﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;

namespace FileVal {
    /// <summary>
    /// The main class for overall interaction with <see cref="FileVal"/>
    /// </summary>
    public class FileValues {
        /// <summary>
        /// Constructor for <see cref="FileValues"/>
        /// </summary>
        /// <param name="filepath">Path for file to read and write</param>
        /// <param name="ResourceFileLinesStream">Default stream of lines for file restore</param>
        /// <param name="isReadOnly">Specifies if this class can write to file</param>
        public FileValues(string filepath, string ResourceFileLinesStream, bool isReadOnly = false) {
            IsReadOnly = isReadOnly;
            if (IsReadOnly && !File.Exists(filepath)) {
                throw new FileValException(FileValException.ExceptionType.ReadOnlyFileMissing, $"File {filepath} not found");
            }
            FileHandlerInstance = new FileHandler(filepath, ResourceFileLinesStream);
        }

        /// <summary>
        /// Constructor for <see cref="FileValues"/>
        /// </summary>
        /// <param name="filepath">Path for file to read and write</param>
        /// <param name="ResourceFileLines">Default line array for file restore</param>
        /// <param name="isReadOnly">Specifies if this class can write to file</param>
        public FileValues(string filepath, string[] ResourceFileLines, bool isReadOnly = false) {
            IsReadOnly = isReadOnly;
            if (IsReadOnly && !File.Exists(filepath)) {
                throw new FileValException(FileValException.ExceptionType.ReadOnlyFileMissing, $"File {filepath} not found");
            }
            FileHandlerInstance = new FileHandler(filepath, ResourceFileLines);
        }
        /// <summary>
        /// Constructor for <see cref="FileValues"/>
        /// </summary>
        /// <param name="fileHandler">Instance of <see cref="FileHandler"/> for injecting</param>
        /// <param name="isReadOnly">Specifies if this class can write to file</param>
        public FileValues(FileHandler fileHandler, bool isReadOnly = false) {
            IsReadOnly = isReadOnly;
            FileHandlerInstance = fileHandler;
            if (IsReadOnly && !File.Exists(fileHandler.FilePath)) {
                throw new FileValException(FileValException.ExceptionType.ReadOnlyFileMissing, $"File {fileHandler.FilePath} not found");
            }
        }
        ////===============================PRIVATE VALUES=========================================
        //save multiple temporary cache
        private List<string> _pendigSaveList = new List<string>();
        private List<string> _keyList = new List<string>();
        //========================================================================================

        /// <summary>
        /// This is created FileHandler instance
        /// </summary>
        public FileHandler FileHandlerInstance { get; }
        /// <summary>
        /// Indicates succession of last write to the disk
        /// </summary>
        public bool LastWriteStatus { get; private set; } = false;
        /// <summary>
        /// Defines if the instance is read-only
        /// </summary>
        public bool IsReadOnly { get; }

        //==============================================================
        //
        //                        PRIVATE METHODS
        //
        //==============================================================


        private void ReadOnly() {
            if (IsReadOnly) {
                throw new FileValException(FileValException.ExceptionType.ReadOnly);
            }
        }

        //==============================================================
        //
        //                        PUBLIC METHODS
        //
        //==============================================================

        /// <summary>
        /// Tries to save new value and change <see cref="LastWriteStatus"/>
        /// </summary>
        /// <param name="section">Where the key=value belongs</param>
        /// <param name="key">The overwriting key with new value</param>
        /// <param name="value">The new value</param>
        ///<exception cref="FileValException">When <paramref name="key"/> OR <paramref name="section"/> not exists OR if property <see cref="IsReadOnly"/> is set to true</exception>
        ///<exception cref="ArgumentException"><paramref name="key"/> OR <paramref name="section"/> is empty</exception>
        ///<exception cref="ArgumentNullException"><paramref name="key"/> OR <paramref name="section"/> is null</exception>
        ///<exception cref="IOException">When <paramref name="value"/> cannot be written to file</exception>
        public void SaveSingle(string section, string key, string value) {
            ReadOnly();
            try {
                FileHandlerInstance.WriteSingle(section, key, value);
            } catch (Exception) {
                LastWriteStatus = false;
                throw;
            }
            LastWriteStatus = true;
        }

        /// <summary>
        /// Stores all new values in RAM until the <see cref="ApplyChanges"/> is called
        /// </summary>
        /// <param name="section">The section where the key=value belongs</param>
        /// <param name="key">key where will the value be changed</param>
        /// <param name="value">The new value for change</param>
        ///<exception cref="FileValException">When <paramref name="key"/> OR <paramref name="section"/> not exists OR if property <see cref="IsReadOnly"/> is set to true</exception>
        ///<exception cref="ArgumentException"><paramref name="key"/> OR <paramref name="section"/> is empty</exception>
        ///<exception cref="ArgumentNullException"><paramref name="key"/> OR <paramref name="section"/> is null</exception>
        public void SaveMultiple(string section, string key, string value) {
            ReadOnly();

            if (_pendigSaveList.Count == 0) {
                _pendigSaveList = FileHandlerInstance.FileLines.ToList();
            }

            var lineIndex = FileHandlerInstance.GetKeyIndex(section, key);
            _pendigSaveList[lineIndex] = $"{key}={value}";
        }

        /// <summary>
        /// All new values tries to write to the disk and change <see cref="LastWriteStatus"/>
        /// </summary>
        /// <exception cref="IOException">When the changes cannot be written to file</exception>
        public void ApplyChanges() {
            if (_pendigSaveList.Count == 0) {
                return;
            }
            try {
                FileHandlerInstance.WriteList(_pendigSaveList);
            } catch (Exception) {
                LastWriteStatus = false;
                throw;
            } finally {
                _pendigSaveList.Clear();
            }
            LastWriteStatus = true;
        }

        /// <summary>
        /// Finds key=value pairs under the section
        /// </summary>
        /// <param name="section">all key=value pairs belonged to the section</param>
        /// <returns>Found Dictionary</returns>
        /// <exception cref="FileValException">When section not exists</exception>
        /// <exception cref="ArgumentException">When section name is empty</exception>
        /// <exception cref="ArgumentNullException">When section name is null</exception>
        public Dictionary<string, string> GetSectionDictionary(string section) {
            return FileHandlerInstance.GetFileDictionaryFrom(section);
        }

        /// <summary>
        /// Get specific value from key in file
        /// </summary>
        /// <param name="section">The section where to find</param>
        /// <param name="key">The value from specified key</param>
        /// <returns>Value of specified section and key</returns>
        ///<exception cref="FileValException">When key name OR section name not exists in file</exception>
        ///<exception cref="ArgumentException"><paramref name="key"/> OR <paramref name="section"/> is empty</exception>
        ///<exception cref="ArgumentNullException"><paramref name="key"/> OR <paramref name="section"/> is null</exception>

        public string GetValue(string section, string key) {
            var sectionDictionary = FileHandlerInstance.GetFileDictionaryFrom(section);

            if (key == null) {
                throw new ArgumentNullException(nameof(key));
            }
            if (key == string.Empty) {
                throw new ArgumentException("Key name is not specified", nameof(key));
            }

            if (!sectionDictionary.Keys.ToArray().Contains(key)) {
                throw new FileValException(FileValException.ExceptionType.KeyNotExist) { ParameterName = nameof(key) };
            }
            return sectionDictionary[key];
        }

        /// <summary>
        /// Gets the default value from key in default file stream
        /// </summary>
        /// <param name="section">The section where to find</param>
        /// <param name="key">The value from specified key</param>
        /// <returns>Default value of specified section and key</returns>
        ///<exception cref="FileValException">When key name OR section name not exists in file</exception>
        ///<exception cref="ArgumentException"><paramref name="key"/> OR <paramref name="section"/> is empty</exception>
        ///<exception cref="ArgumentNullException"><paramref name="key"/> OR <paramref name="section"/> is null</exception>
        public string GetDefaultValue(string section, string key) {
            var sectionDictionary = FileHandlerInstance.GetDefaultFileDictionaryFrom(section);

            if (key == null) {
                throw new ArgumentNullException(nameof(key));
            }
            if (key == string.Empty) {
                throw new ArgumentException("Key name is not specified", nameof(key));
            }

            if (!sectionDictionary.Keys.ToArray().Contains(key)) {
                throw new FileValException(FileValException.ExceptionType.KeyNotExist) { ParameterName = nameof(key) };
            }
            return sectionDictionary[key];
        }

        /// <summary>
        /// Restores default file stream to file
        /// </summary>
        ///<exception cref="FileValException">When property <see cref="IsReadOnly"/> is set to true</exception>
        /// <exception cref="IOException">When the default file stream cannot be written to file</exception>
        public void RestoreDefalutFileStream() {
            ReadOnly();
            try {
            FileHandlerInstance.WriteDefaultStream();
            } catch (Exception) {
                LastWriteStatus = false;
                throw;
            }
            LastWriteStatus = true;
        }
    }
}