﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace FileVal {
    /// <summary>
    /// Class for File manipulation
    /// </summary>
    /// 
    public class FileHandler {

        /// <summary>
        /// Constructor for Filehandler
        /// </summary>
        /// <param name="filepath">Path for file</param>
        /// <param name="defaultFileLinesStream">Default line stream</param>
        public FileHandler(string filepath, string defaultFileLinesStream) {
            if (!FileExists(filepath)) {
                File.WriteAllText(filepath, defaultFileLinesStream, System.Text.Encoding.UTF8);
            }
            FilePath = filepath;
            if (String.IsNullOrEmpty(defaultFileLinesStream)) {
                throw new ArgumentNullException("defaultFileLinesStream", "String with default parameters could not be empty");
            }
            _defaultLines = GetLineArrayFromLinesStream(defaultFileLinesStream);
            _defaultSections = AllocateSections(_defaultLines);
            LoadFileContents();
            CheckSectionEquality();
           
        }

        /// <summary>
        /// Constructor for Filehandler
        /// </summary>
        /// <param name="filepath">Path for file</param>
        /// <param name="defaultFileLines">Default line array</param>
        public FileHandler(string filepath, string[] defaultFileLines) {
            if (!FileExists(filepath)) {
                string str = null;
                for (int i = 0; i < defaultFileLines.Length; i++) {
                    str += defaultFileLines[i];
                    if (i < defaultFileLines.Length - 1) {
                        str += "\n";
                    }
                }
                File.WriteAllText(filepath, str, System.Text.Encoding.UTF8);
            }
            FilePath = filepath;
            if (defaultFileLines.Length == 0) {
                throw new ArgumentNullException(nameof(defaultFileLines), "Array with default parameters could not be empty");
            }
            _defaultLines = defaultFileLines;
            CheckSectionEquality();
            _defaultSections = AllocateSections(_defaultLines);
            LoadFileContents();
        }

        private const string REGEX_KEYVALUE_PAIR = @"^[a-zA-Z][a-zA-Z_0-9]+=.*";
        private const string REGEX_SECTION = @"^[[][a-zA-Z0-9]+[]]";
        private List<FileSection> _sections = new List<FileSection>();
        private List<FileSection> _defaultSections = new List<FileSection>();
        private string[] _defaultLines;
        //==============================================================================



        //=======================================PUBLIC PROPERTIES=========================================
        /// <summary>
        /// Returns fetched FileLines
        /// </summary>
        public string[] FileLines { get; private set; }
        /// <summary>
        /// This is constant regex key=value pair pattern
        /// </summary>
        public string RegexKeyValuePattern => REGEX_KEYVALUE_PAIR;
        /// <summary>
        /// This is constant regex [section] pattern
        /// </summary>
        public string RegexSectionPattern => REGEX_SECTION;
        /// <summary>
        /// Path where are stored key=values in file
        /// </summary>
        public string FilePath { get; }

        //=================================================================================================

        //==============================================================
        //
        //                        PRIVATE METHODS
        //
        //==============================================================

        #region PRIVATE METHODS
        /// <summary>
        /// Will read the file and analyze it's contents for indexing
        /// </summary>
        public void LoadFileContents() {
            FileLines = File.ReadAllLines(FilePath);
            _sections = AllocateSections(FileLines);
        }

        private void CheckSectionEquality() {
            var defaultSections = GetAllFileDefaultSections();
            var sections = GetAllFileSections();

            
            if (!defaultSections.SetEquals(sections)) {
                throw new FileValException(FileValException.ExceptionType.SectionsNotMatchInDefaultStream);
            }
        }

        private FileSection GetSection(string section) {
            if (section == null) {
                throw new ArgumentNullException(nameof(section));
            }

            if (section == string.Empty) {
                throw new ArgumentException("Section name not specified", nameof(section));
            }

            foreach (var currSection in _sections) {
                if (currSection.SectionName == section) {
                    return currSection;
                }
            }

            throw new FileValException(FileValException.ExceptionType.SectionNotExist) { ParameterName = nameof(section) };
        }

        private List<FileSection> AllocateSections(string[] lines) {
            var sections = new List<FileSection>();

            for (int i = 0; i < lines.Length; i++) {
                var line = lines[i];
                var sectionName = GetSectionName(line);
                if (String.IsNullOrEmpty(sectionName)) continue;

                var sectionIndex = i;
                var dict = GetSectionPairs(lines, ref i);

                var section = new FileSection(sectionName, dict, sectionIndex);
                sections.Add(section);
            }
            return sections;
        }

        private Dictionary<int, KeyValuePair<string, string>> GetSectionPairs(string[] lines, ref int i) {
            var keys = new HashSet<string>();
            var reg = new Regex(REGEX_SECTION, RegexOptions.Compiled);
            var dict = new Dictionary<int, KeyValuePair<string, string>>();

            if (i + 1 >= lines.Length) return dict;
            
            i++; //moving one down for pair
            var m = reg.Match(lines[i]);

            while (!m.Success) {
                var line = lines[i];
                if (HasKeyValuePair(line)) {
                    var pair = GetKeyValue(line);
                    if (keys.Contains(pair.Key)) {
                        throw new FileValException(FileValException.ExceptionType.DuplicateKey);
                    } else {
                        keys.Add(pair.Key);
                    }
                    dict.Add(i, pair);
                }
                i++;
                if (i>=lines.Length) break;
                m = reg.Match(lines[i]);
            }
            i--; // returning value back after work
            return dict;
        }

        private string GetSectionName(string line) {
            var m = Regex.Match(line, REGEX_SECTION);
            if (m.Success) {
                return m.Value.Substring(1, m.Value.Length - 2);
            } else {
                return null;
            }
        }

        private Dictionary<string, string> InternalGetFileDictionary(string section, List<FileSection> sectionsCollection) {
            return GetSection(section).KeyValues;
        }

        private KeyValuePair<string, string> GetKeyValue(string line) {
            var m = Regex.Match(line, @"^(?<key>[a-zA-Z][a-zA-Z_0-9]+)=(?<value>.*)");
            var key = m.Groups["key"].Value;
            var value = m.Groups["value"].Value;

            if (m.Success) {
                return new KeyValuePair<string, string>(key, value);
            } else {
                return new KeyValuePair<string, string>(null, null);
            }
        }

        private bool FileExists(string file) {
            return File.Exists(file);
        }

        private bool HasKeyValuePair(string line) {
            Match m = Regex.Match(line, REGEX_KEYVALUE_PAIR);
            return m.Success;

        }

        private string[] GetLineArrayFromLinesStream(string lineStream) {
            return lineStream.Split(new string[] { "\n" }, StringSplitOptions.None);
        }


        private HashSet<string> InternalGetAllFileSections(List<FileSection> sections) {
            var sectionNames = new HashSet<string>();
            foreach (var section in sections) {
                if (sectionNames.Contains(section.SectionName)) {
                    throw new FileValException(FileValException.ExceptionType.DuplicateSections);
                }
                sectionNames.Add(section.SectionName);
            }
            return sectionNames;
        }

        private Dictionary<string, Dictionary<string, string>> GetGeneralDirectory(List<FileSection> sections) {
            Dictionary<string, string> innerDictionary = new Dictionary<string, string>();
            Dictionary<string, Dictionary<string, string>> outerDictionary = new Dictionary<string, Dictionary<string, string>>();

            foreach (var section in sections) {
                innerDictionary = section.KeyValues;
                outerDictionary.Add(section.SectionName, innerDictionary);
            }

            return outerDictionary;
        }

        private void InternalWriteAllLines(string path, string[] lines, Encoding encoding) {
            if (path == null)
                throw new ArgumentNullException(nameof(path));
            if (lines == null)
                throw new ArgumentNullException(nameof(lines));
           
            using (StreamWriter writer = new StreamWriter(path, false, encoding)) {
                if (lines.Length > 0) {
                    for (int i = 0; i < lines.Length - 1; i++) {
                        writer.WriteLine(lines[i]);
                    }
                    writer.Write(lines[lines.Length - 1]);
                }
            }
        }
        #endregion


        //==============================================================
        //
        //                        PUBLIC METHODS
        //
        //==============================================================

        #region PUBLIC METHODS
        /// <summary>
        /// Makes overall section sorted dictionary with all key=values pairs from file
        /// </summary>
        /// <returns>section as Key and 'Dictionary of key=value' as value</returns>
        public Dictionary<string, Dictionary<string, string>> GetFileDictionary() {
            return GetGeneralDirectory(_sections);
        }

        /// <summary>
        /// Makes overall section sorted dictionary with all key=values pairs from default file
        /// </summary>
        /// <returns>section as Key and 'Dictionary of key=value' as value</returns>
        public Dictionary<string, Dictionary<string, string>> GetDefaultFileDictionary() {
            return GetGeneralDirectory(_defaultSections);
        }

        /// <summary>
        /// Dictionary of all keys=value pair with section in <see cref="FileLines"/>
        /// </summary>
        /// <param name="section">keys and values related to the section</param>
        /// <returns>Dictionary of Keys and Values</returns>
        /// <exception cref="FileValException">When section not exists</exception>
        /// <exception cref="ArgumentException">When section name is empty</exception>
        /// <exception cref="ArgumentNullException">When section name is null</exception>
        public Dictionary<string, string> GetFileDictionaryFrom(string section) {
            return InternalGetFileDictionary(section, _sections);
        }

        /// <summary>
        /// Dictionary of all keys=value pair with section in <see cref="_defaultLines"/>
        /// </summary>
        /// <param name="section">keys and values related to the section</param>
        /// <returns>Dictionary of Keys and Values</returns>
        /// <exception cref="FileValException">When section not exists</exception>
        /// <exception cref="ArgumentException">When section name is empty</exception>
        /// <exception cref="ArgumentNullException">When section name is null</exception>
        public Dictionary<string, string> GetDefaultFileDictionaryFrom(string section) {
            return InternalGetFileDictionary(section, _defaultSections);
        }

        /// <summary>
        /// Finds all file sections
        /// </summary>
        /// <returns>Collection of sections sorted as they are written in <see cref="FilePath"/></returns>
        public HashSet<string> GetAllFileSections() {

            return InternalGetAllFileSections(_sections);
        }

        /// <summary>
        /// Finds all file sections
        /// </summary>
        /// <returns>An array of string sections sorted as they are written in <see cref="FilePath"/></returns>
        public HashSet<string> GetAllFileDefaultSections() {
            return InternalGetAllFileSections(_defaultSections);
        }

        /// <summary>
        /// Tries to write a new value on specified key
        /// </summary>
        /// <param name="section">section where to write</param>
        /// <param name="key">key in specified section</param>
        /// <param name="value">new value</param>
        ///<exception cref="FileValException">When key name OR section name not exists in file</exception>
        ///<exception cref="ArgumentException"><paramref name="key"/> OR <paramref name="section"/> is empty</exception>
        ///<exception cref="ArgumentNullException"><paramref name="key"/> OR <paramref name="section"/> is null</exception>
        ///<exception cref="IOException">When <paramref name="value"/> cannot be written to file</exception>
        public void WriteSingle(string section, string key, string value) {
            var sectionObj = GetSection(section);

            if (key == string.Empty) {
                throw new ArgumentException("Key name is not specified", nameof(key));
            }

            if (key == null) {
                throw new ArgumentNullException(nameof(key));
            }

            if (!sectionObj.KeyIndexes.Keys.Contains(key)) {
                throw new FileValException(FileValException.ExceptionType.KeyNotExist);
            }



            // Writes new value to key
            var index = sectionObj.KeyIndexes[key];
            FileLines[index] = $"{key}={value}";
            InternalWriteAllLines(FilePath, FileLines, Encoding.UTF8);
            sectionObj.UpdateKey(key, value);
        }
        /// <summary>
        /// Finds index of specified key in specified section
        /// </summary>
        /// <param name="section">Section where is the key</param>
        /// <param name="key">Key which index should be found</param>
        /// <returns>A number representing an index of key</returns>
        ///<exception cref="FileValException">When key name OR section name not exists in file</exception>
        ///<exception cref="ArgumentException"><paramref name="key"/> OR <paramref name="section"/> is empty</exception>
        ///<exception cref="ArgumentNullException"><paramref name="key"/> OR <paramref name="section"/> is null</exception>
        public int GetKeyIndex(string section, string key) {
            var sectionObj = GetSection(section);

            if (key == string.Empty) {
                throw new ArgumentException("Key name is not specified", nameof(key));
            }

            if (key == null) {
                throw new ArgumentNullException(nameof(key));
            }

            if (!sectionObj.KeyIndexes.Keys.Contains(key)) {
                throw new FileValException(FileValException.ExceptionType.KeyNotExist) { ParameterName = nameof(key) };
            }

            return sectionObj.KeyIndexes[key];
        }

        /// <summary>
        /// Tries to write entire list to <see cref="FilePath"/>
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="IOException">When the list cannot be written to file</exception>
        public void WriteList(List<string> list) {
            if (list == null) {
                throw new ArgumentNullException(nameof(list));
            }

            InternalWriteAllLines(FilePath, list.ToArray(), Encoding.UTF8);

            FileLines = list.ToArray();
            _sections = AllocateSections(FileLines);
            CheckSectionEquality();
            
        }
        /// <summary>
        /// Will update value of specified key in <see cref="FileHandler"/>'s internal file index
        /// </summary>
        /// <param name="section">Section where is the key</param>
        /// <param name="key">The key for value update</param>
        /// <param name="value">New value for key</param>
        ///<exception cref="FileValException">When key name OR section name not exists in file</exception>
        ///<exception cref="ArgumentException"><paramref name="key"/> OR <paramref name="section"/> is empty</exception>
        ///<exception cref="ArgumentNullException"><paramref name="key"/> OR <paramref name="section"/> is null</exception>
        public void UpdateKey(string section, string key, string value) {
            var obj = GetSection(section);

            obj.UpdateKey(key, value);
        }

        /// <summary>
        /// Writes default file stream to file
        /// </summary>
        /// <exception cref="IOException">When the default file stream cannot be written to file</exception>
        public void WriteDefaultStream() {
            InternalWriteAllLines(FilePath, _defaultLines, Encoding.UTF8);
            FileLines = _defaultLines;
        }
        #endregion
    }
}
